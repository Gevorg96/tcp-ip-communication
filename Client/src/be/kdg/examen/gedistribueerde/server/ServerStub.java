package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.Document;
import be.kdg.examen.gedistribueerde.client.DocumentImpl;
import be.kdg.examen.gedistribueerde.client.DocumentSkeleton;
import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

import javax.print.Doc;

public class ServerStub implements Server {

    private final NetworkAddress serverAddress;
    private final MessageManager messageManager;
    private final NetworkAddress myadress;
    private final  DocumentSkeleton documentSkeleton ;
    //om een massage te sturen naar het server, we hebben IP adress van dat server en onze eigen IP address nodig
    //dit vraag ik aan object die server aanspreekt
    public ServerStub(NetworkAddress serverAddress) {
        this.serverAddress = serverAddress;
        //manager zorgt voor correcte messages tussen twee TCP/IP poten via ServerSocket
        this.messageManager = new MessageManager();
        //bij het creëren van messageManager hebben we onze eigen IP gekregen in dat Object
        this.myadress = messageManager.getMyAddress();
       this.documentSkeleton = new DocumentSkeleton(messageManager);

    }

    @Override
    public void log(Document document) {
        //we maken een methodCallmessage met onze ipaddress en een methodName
        //om dit over netwerk te sturen
        MethodCallMessage message = new MethodCallMessage(myadress,"log");
        message.setParameter("text", (document.getText()));
        messageManager.send(message,serverAddress);
        System.out.println("Server met IP: " + serverAddress + " zal mijn message (Document) loggen ");
        System.out.println( checkReply());

    }

    @Override
    public Document create(String s) {

      /*  Document document = new DocumentImpl();
        document.setText("---" + s + "---");
        return document;*/

        // createn van document via server
        MethodCallMessage message = new MethodCallMessage(this.myadress,"create");
        message.setParameter("text", ("---" + s + "---"));
        messageManager.send(message, serverAddress);

        //wachten tot server een reply terugstuurt (dit reply is stringvorm van Document)
        return new DocumentImpl(checkReply());
    }

    @Override
    public void toUpper(@CallByRef Document document) {
        MethodCallMessage message = new MethodCallMessage(this.myadress,"toUpper");
        message.setParameter("text", document.getText());
        messageManager.send(message, serverAddress);

        //wachten tot server een reply terugstuurt
        System.out.println( checkReply());
        //update Document
        documentSkeleton.Update(document);
        System.out.println(document);
    }

    @Override
    public void toLower(Document document) {
        MethodCallMessage message = new MethodCallMessage(this.myadress,"toLower");
        message.setParameter("text", document.getText());
        messageManager.send(message, serverAddress);

        //wachten tot server een reply terugstuurt
        System.out.println( checkReply());
        //update Document
        documentSkeleton.Update(document);
        System.out.println(document);
    }

    @Override
    public void type(Document document, String text) {
        MethodCallMessage message = new MethodCallMessage(this.myadress,"type");
        message.setParameter("text", document.getText());
        message.setParameter("typetext", text);
        messageManager.send(message, serverAddress);

        //wachten tot server een reply terugstuurt
        System.out.println( checkReply());

        //update Document
        documentSkeleton.Update(document);
        System.out.println(document);
    }



    private String checkReply() {
        MethodCallMessage reply = messageManager.wReceive();
        if ("replyFromServer".equals(reply.getMethodName())) {
            System.out.println("From Server : status =" + reply.getParameter("status"));
            return reply.getParameter("result");
        }
        else return null;
    }



}