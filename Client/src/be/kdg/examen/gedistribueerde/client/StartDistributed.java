package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;
import be.kdg.examen.gedistribueerde.server.Server;
import be.kdg.examen.gedistribueerde.server.ServerStub;

public class StartDistributed {
    public static void main(String[] args) {

        //Create the skeleton
        int serverPort = Integer.parseInt(args[1]);
        MessageManager gs = new MessageManager();
        NetworkAddress serverAddress = new NetworkAddress(args[0], serverPort);

        Server server = new ServerStub(serverAddress);
        DocumentImpl document = new DocumentImpl();
        Client client = new Client(server, document);
        client.run();
    }
}
