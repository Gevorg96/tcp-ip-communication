package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.server.CallByRef;

public class DocumentSkeleton {
    private final MessageManager messageManager;
    public DocumentSkeleton(MessageManager messageManager) {
        this.messageManager = messageManager;
    }

    public void Update(@CallByRef Document document) {
        //blijven updaten tot status updated teruggeeft
        var status = "";
        while (!"updated".equals(status)) {
            MethodCallMessage reply = messageManager.wReceive();
         if ("Update".equals(reply.getMethodName())) {
             // System.out.println(reply.getParameter("status"));
             document.setText(reply.getParameter("result"));
         }else {
             status = reply.getParameter("status");
             System.out.println(reply.getParameter("status"));
         }
        }
    }
}
