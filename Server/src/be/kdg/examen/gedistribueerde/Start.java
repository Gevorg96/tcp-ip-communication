package be.kdg.examen.gedistribueerde;

import be.kdg.examen.gedistribueerde.server.ServerSkeleton;

public class Start {

    public static void main(String[] args) {
        ServerSkeleton chatServerSkeleton = new ServerSkeleton();
        chatServerSkeleton.run();
    }
}
