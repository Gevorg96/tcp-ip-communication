package be.kdg.examen.gedistribueerde.server;


import be.kdg.examen.gedistribueerde.client.ClientStub;
import be.kdg.examen.gedistribueerde.client.Document;
import be.kdg.examen.gedistribueerde.client.DocumentImpl;
import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

import java.util.Date;

public class ServerSkeleton {
    private final MessageManager messageManager;
    private Server serverImpl;
    private Document document;
    private ClientStub clientStub ;

    public ServerSkeleton() {
        //tijdens het maken van server maken we ook messageManager
        //om massages te ontvangen in ServerSkelleton
        this.messageManager = new MessageManager();
        this.serverImpl = new ServerImpl();
        this.clientStub =  new ClientStub(messageManager);
    }


    public void run() {
        //Ip adress van deze Server tonen en eindlos wachten op inkomende berichten
        System.out.println("IP address of the server is: " + messageManager.getMyAddress());
        while (true) {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }
    }


    private void handleRequest(MethodCallMessage request){
        //Get a request and send it to a certain handler.
        String methodName = request.getMethodName();
        if ("log".equals(methodName)) {
            serverImpl.log(new DocumentImpl(request.getParameter("text")));
            //reply sturen naar client zodat hij weet dat log goed is gelukt en status is logged
            sendReply("Log is gelukt","logged",request.getOriginator());

        } else if ("create".equals(methodName)) {
            this.document = new DocumentImpl(request.getParameter("text"));
            //client request (document) is aangemaakt nu moeten we client op de hogte houden
            //ofwel sturen we een ok reply ofwel kunnene we die text van document trugsturen
            sendReply(document.getText(),"created",request.getOriginator());

        } else if ("toUpper".equals(methodName)) {
            this.document = new DocumentImpl(request.getParameter("text"));
            this.clientStub.setClientInfo(request.getOriginator(), this.document);
            ((DocumentImpl) this.document).setTextListener(this.clientStub);
            sendReply("Uw document word behandeld (alles word in hoofdletter gezet)","ToUppercase",request.getOriginator());
            serverImpl.toUpper(document);
            clientStub.updated();
        }
        else if ("toLower".equals(methodName)) {
            this.document = new DocumentImpl(request.getParameter("text"));
            this.clientStub.setClientInfo(request.getOriginator(), this.document);
            ((DocumentImpl) this.document).setTextListener(this.clientStub);
            sendReply("Uw document word behandeld (alles word in kleinletter gezet)","ToLowercase",request.getOriginator());
            serverImpl.toLower(document);
            clientStub.updated();

        } else if ("type".equals(methodName)) {
            this.document = new DocumentImpl(request.getParameter("text"));
            this.clientStub.setClientInfo(request.getOriginator(), this.document);
            ((DocumentImpl) this.document).setTextListener(this.clientStub);
            sendReply("Uw text word geschreven in document","ToLowercase",request.getOriginator());
            serverImpl.type(document, request.getParameter("typetext"));
            clientStub.updated();
        }


        else {
            System.out.println("ChatServerSkeleton: received an unknown request:");
            System.out.println(request);
        }
    }







    private void sendReply(String message , String status, NetworkAddress address) {
        //Sends a empty reply to the request maker.

        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "replyFromServer");
        reply.setParameter("result", message);
        reply.setParameter("status", status);

        messageManager.send(reply, address);
    }


}
