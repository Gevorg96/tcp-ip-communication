package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;
import be.kdg.examen.gedistribueerde.server.CallByRef;

import java.awt.event.TextEvent;
import java.awt.event.TextListener;

public class ClientStub implements TextListener {
    private  NetworkAddress clientAdress;
    private  MessageManager messageManager;
    private Document document;
    public ClientStub(MessageManager messageManager) {
        this.messageManager = messageManager;
        System.out.println(messageManager.getMyAddress());
    }
    public void setClientInfo(NetworkAddress clientAdress, Document document) {
    this.clientAdress = clientAdress;
    this.document = document;
    }


    public void updateClientsDocument(String text) {
            //Sends a empty reply to the request maker.
            MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "Update");
            reply.setParameter("result", text);
            reply.setParameter("status", "updating");
            messageManager.send(reply, this.clientAdress);
    }
    public void updated() {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "replyFromServer");
        reply.setParameter("status", "updated");
        reply.setParameter("result", document.getText());

        messageManager.send(reply, this.clientAdress);

    }

    @Override
    public void textValueChanged(TextEvent e) {
        updateClientsDocument(document.getText());
    }
}
